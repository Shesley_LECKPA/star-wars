<?php
        session_start();
        $titre_page = "STAR WARS - Connexion";
        require_once('header.inc.php');
?>
<body background="images/imagefond.jpg"> 
    <h1 class="centerpersonnage">Authentification</h1>
    <div class="row centerdiv">
        <form method="post"  class="center">
            <div class="form-group row">
                <label class="col-6" for="identifiant">Identifiant:</label>
                <div class="col-6"> 
                    <input class="form-control" type="text" id="identifiant" size="50" maxlength="50" name="email" required/><br />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-6" for=password>Mot de passe:</label> 
                <div class="col-6">
                    <input class="form-control" type="password" id="password" size="50" maxlength="50" name="pass" required/><br />
                </div>
            </div>
            <div class="">
                <input class="btn btn-primary" name="formsend" type="submit" value="Connexion" />
            </div>
            <div class="row"><br/></div>
        </form>
        <?php
            if(isset($_POST['formsend'])){
                extract($_POST);
                if(!empty($email) && !empty($pass)){
                    require_once("param.inc.php");
                    $mysqli = new mysqli($host, $login, $password, $dbname);
                        if ($mysqli->connect_errno){ 
                            echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
                        }else{
                            $stmt = $mysqli->prepare("SELECT * FROM utilisateur WHERE email = ?");
                            $stmt->bind_param('s',$email);
                            $stmt->execute();
                            $result = $stmt->get_result();
                            $row = $result->fetch_assoc();
                            if($row == true){
                                if(!password_verify($pass, $row['password'])){
                                    ?>
                                    <div class = "row center">
                                        <div class = "alert alert-danger" role = "alert">
                                            Mot de passe incorrect ! <a href="connexion.php">Réessayer</a>
                                           
                                        </div>
                                    </div>  
                                    <?php
                                }else{
                                    $_SESSION['connect'] = "ok";
                                    $_SESSION['id_user'] = $row['id'];
                                    $_SESSION['nom'] = $row['nom'];
                                    $_SESSION['prenom'] = $row['prenom'];
                                    $_SESSION['email'] = $row['email'];
                                    $_SESSION['role'] = $row['role'];
                                    
                                    header('Location:index.php');
                                    ?>
                                    <div class = "row center">
                                        <div class = "alert alert-success" role = "alert">
                                            <a href="index.php">Retour à la page d'accueil</a>
                                        </div>
                                    </div>  
                                    <?php
                                }
                            }else{
                                ?>
                                <div class = "row center">
                                    <div class = "alert alert-danger" role = "alert">
                                        Ce compte n'existe pas ! <a href="connexion.php">Réessayer</a>
                                        Vous n'avez pas encore de compte? <a href="insciption.php">Créer un compte</a>
                                    </div>
                                </div>  
                                <?php
                            }
                        }

                }
            }

        ?>

        
                               
    </div>
    <?php require_once("footer.inc.php"); ?>
</body>