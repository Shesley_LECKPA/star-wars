<?php
  session_start();
  $titre_page = "STAR WARS - Film - Modifier - Personnage";
  if((!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok") || $_SESSION['role']===0){
    header('Location:index.php');
    exit;
  }
    require('header.inc.php');
?>
<body>
    <?php
        require_once("param.inc.php");
        $mysqli = new mysqli($host, $login, $password, $dbname);
        if(isset($_POST['modifier'])){
            $id=$_POST['modifier_id'];
            $result = $mysqli->query("SELECT * FROM personnage WHERE id = $id");
            $row = $result->fetch_assoc();
    ?>
    <h3 class="centerpersonnage">Modification du personnage</h3><br>
    <div class="row">
    <form method="POST"  action="modifierPers.php"  enctype="multipart/form-data" class = "center">
     <div class="form-group row">
       <label for="identifiant">Nom</label>
       <input type="text" class="form-control" name="nom" id="nom" value=<?php echo $row['nom'];?> placeholder="nom du personnage"  required>
     </div>
     <div class="form-group row">
       <label for="identifiant">Description</label>
       <textarea class="form-control " name="description" id="description" required> <?php echo $row['description'];?></textarea>
     </div>
     <div class="form-group row">
       <label for="identifiant">Image </label>
       <input type="file" class="form-control" name="image" id="image" required>
     </div>
     <input type="hidden" name="modifier_id" id="modifier_id"  value="<?php echo $id;?>">
     <button type="submit" name="valider" id="valider" class="btn btn-primary">Valider</button>
     <button type="submit" name="annuler" id="annuler" class="btn btn-primary">Annuler</button>
    </form>
    </div>
    <?php
        }
    ?>
    <?php require_once("footer.inc.php"); ?>
</body>