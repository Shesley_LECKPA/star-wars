<?php
    session_start();
    $titre_page = "STAR WARS - Rechercher";
    if(!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok"){
        header('Location:index.php');
        exit;
    }
    require('header.inc.php');
?>
<body>
    <?php
        if(isset($_GET['rechercher'])){
            require_once('param.inc.php');
            $mysqli = new mysqli($host, $login, $password, $dbname);
            $q = $_GET['recherche'];
            if(strlen($q)>0){
                $s = explode(" ", $q);
                $sql = "SELECT * FROM film";
                $i = 0;
                foreach($s as $mot){
                    if(strlen($mot) > 3){
                        if($i==0){
                            $sql.=" WHERE ";
                        }else{
                            $sql.=" OR ";
                        }
                        $sql.=" titre LIKE '%$mot%'";
                        $i++;
                    }
                }
                $result = $mysqli->query("$sql");
                $row = $result->fetch_assoc();
                $nbr = mysqli_num_rows($result);
                if($nbr == 0){
                    echo "<h4>Aucun résultat </h4>";
                }else{
                    while($row == true){
                        ?>
                        
                        <div class="row  taillerow espacefilm defilement">
                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12 taillecol">
                                <img class="image-fluid" src="<?php echo 'images/'.$row['image']; ?>" height=100% width=100% />
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-12 cadre">
                                <h4><?php echo $row['titre']; ?></h4>
                                <h6>Episode <?php echo $row['episode']; ?></h6>
                                <p> Résumé: </p> 
                                <p><?php echo $row['description']; ?></p><br/>
                                <p> Date de sortie : <?php echo $row['date_sortie']; ?></p><br/>
                                <h6><p> Liste des personnages </p></h6>
                                <?php
                                $id_film = $row['id'];
                                $result1 = $mysqli->query("SELECT * FROM acteurfilm WHERE id_film = $id_film");
                                $row1 = $result1->fetch_assoc();
                                $a = 1;
                                while($row1 == true){
                                    $id_pers = $row1['id_personnage'];
                                    $result2 = $mysqli->query("SELECT * FROM personnage WHERE id = $id_pers");
                                    $row2 = $result2->fetch_assoc();
                                    while($row2 == true){
                                        if($a == 1){
                                            $a = 0;
                                            echo $row2['nom'];
                                        }else{
                                            echo ", ".$row2['nom'];
                                        }
                                        $row2 = $result2->fetch_assoc();  
                                         
                                    }
                                    
                                    $row1 = $result1->fetch_assoc();
                                }
                                ?>
                            </div>
                        </div>
                            
                            
                        
                        <?php
                       $row = $result -> fetch_assoc(); 
                        
                    }
                }
                
            
            }
        }
    ?>
</body>