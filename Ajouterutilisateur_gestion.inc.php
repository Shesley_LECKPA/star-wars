<?php  
    session_start();
    
    if(isset($_POST['ajoututilisateur'])){
        extract($_POST);
        if(!empty($nom) && !empty($prenom) && !empty($email) && !empty($pass) && ($role == 0 || $role == 1)){
            $options = [
                'cost' => 10
            ];
            $hashpass = password_hash($pass, PASSWORD_BCRYPT, $options);
            require_once("param.inc.php");
            $mysqli = new mysqli($host, $login, $password, $dbname);
            if ($mysqli->connect_errno){ 
                $_SESSION['erreursql'] = "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                header('Location: gestion.php?erreursql=erreursql');
            }else{
                $c = $mysqli->prepare("SELECT * FROM utilisateur WHERE email = ?");
                $c->bind_param('s',$email);
                $c->execute();
                $result = $c->get_result();
                $nbr = mysqli_num_rows($result);
                if($nbr == 0){
                    $stmt = $mysqli->prepare("INSERT INTO utilisateur(nom, prenom, email, password, role) VALUES (?, ?, ?, ?, ?)");
                    $stmt->bind_param('ssssi',$nom, $prenom, $email, $hashpass, $role);
                    $stmt->execute();
                    $_SESSION['ajoutreussi'] = 'L\'utilisateur a bien été ajouté.';
                    header('Location: gestion.php?ajoutreussi=ajoutreussi');
                    
                }else{ 
                    $_SESSION['utilisateurexistant'] = 'Cet utilisateur existe déja.';
                    header('Location: gestion.php?utilisateurexistant=utilisateurexistant');                        
                }
                
            }
        }else{
            $_SESSION['champvide'] = 'Champs vides.';
            header('Location: gestion.php?champvide=champvide');
        }
    }
    
?>