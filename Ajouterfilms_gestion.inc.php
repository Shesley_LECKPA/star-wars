<?php  
    session_start();   

if(isset($_POST['ajoutfilm'])){
    $image = $_FILES["image"]['name'];
   
}

if(isset($_POST['ajoutfilm'])) {

    
    extract($_POST);        
    if(!empty($titre) && !empty($date_sortie) && !empty($episode) && !empty($description)){           
        require_once("param.inc.php");
        $mysqli = new mysqli($host, $login, $password, $dbname);
        if ($mysqli->connect_errno){ 
            $_SESSION['erreursql'] = "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            header('Location: gestion.php?erreursql=erreursql');
        }else{
            $c = $mysqli->prepare("SELECT * FROM film WHERE titre = ?");
            $c->bind_param('s',$titre);
            $c->execute();
            $result = $c->get_result();
            $nbr = mysqli_num_rows($result);
            if(file_exists("images/".$_FILES['image']['name'])) { 
                if($nbr == 0){  
                    define('HOST','localhost');
                    define('DB_NAME','starwars');
                    define('USER','root');
                    define('PASS','root');
            
                    try{
                        $db = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME, USER,  PASS);
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        
                    } catch(PDOException $e){
                        echo $e;
                    }
            
                        $data = $db->prepare(" INSERT INTO film (titre,date_sortie,episode,description,image) VALUES(:titre,:date_sortie,:episode,:description,:image)");
                        $data->execute([	
                            'titre'=> $titre,
                            'date_sortie'=> $date_sortie,
                            'episode'=> $episode,
                            'description'=>$description,
                            'image'=>$image,
                        ]);
                        move_uploaded_file($_FILES['image']['name'], "images/".$_FILES['image']['name']);
                                    
                    $_SESSION['ajoutreussi'] = 'Le film a bien été ajouté.';
                    header('Location: gestion.php?ajoutreussi=ajoutreussi');
                    
                }else{ 
                    $_SESSION['filmexistant'] = 'Ce film existe déja.';
                    header('Location: gestion.php?filmexistant=filmexistant');                        
                }
            }
            
        }
    }else{
        $_SESSION['champvide'] = 'Champs vides.';
        header('Location: gestion.php?champvide=champvide');
    }
}
    
?>