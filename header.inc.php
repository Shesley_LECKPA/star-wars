<?php
    if(session_status()===PHP_SESSION_NONE) {
        session_start();
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo($titre_page); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src=" https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
    <script src="js/films.js" defer></script>
</head>
    <div class="bg-dark sticky-top">
        <header class="nav navbar navbar-expand navbar-static-top">
            <div class="container-fluid navbar-head">
                <nav class="navbar navbar-collapse bg-dark">
                    <ul class="nav navbar-nav navbar-left">
                        <img class="img-fluid" src="images/star wars logoR.jpg" max-width/>
                    </ul>
                    <ul class="nav navbar-nav">
                        <?php
                        if(isset($_SESSION['connect']) && $_SESSION['connect'] === "ok") {
                            echo ' 
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Accueil</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="films.php">Films</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="personnages.php">Personnages</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="vote.php">Vote</a>
                        </li>';
                            if($_SESSION['role']===1) { 
                                echo ' 
                                <li class="nav-item active">
                                    <a class="nav-link" href="gestion.php">Gestion</a>
                                </li> ';
                            }
                            echo ' 
                        <li>
                            <form class="form-inline" action="search.php" method="get">
                                <input class="form-control" type="text" id="recherche" maxlength="70" name="recherche" placeholder="Rechercher un film"/>&nbsp;&nbsp;&nbsp;
                                <input class="btn btn-success" name="rechercher" type="submit" value="Rechercher" />
                            </form>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="couleurtitre">
                            <div class="col text-right">
                                <div class="dropdown">
                                    <button type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown">
                                        '.$_SESSION["prenom"]. " " . $_SESSION["nom"].'
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item text-danger" href="deconnexion.php">Déconnexion</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        ';}else {
                            echo '
                            <li>
                                <a class="btn btn-primary" href="connexion.php">Connexion</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </li>
                            <li>
                                <a class="btn btn-success" href="insciption.php">Inscription</a>
                            </li>
                            ';
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </header>
    </div> 
