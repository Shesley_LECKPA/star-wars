-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 27 Novembre 2020 à 00:48
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `starwars`
--

-- --------------------------------------------------------

--
-- Structure de la table `acteurfilm`
--

CREATE TABLE `acteurfilm` (
  `id_personnage` int(6) UNSIGNED NOT NULL,
  `id_film` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `acteurfilm`
--

INSERT INTO `acteurfilm` (`id_personnage`, `id_film`) VALUES
(1, 1),
(1, 11),
(1, 12),
(1, 13),
(2, 1),
(2, 2),
(2, 12),
(2, 14),
(3, 1),
(3, 2),
(3, 3),
(3, 13),
(3, 15),
(4, 2),
(4, 3),
(4, 4),
(4, 14),
(5, 3),
(5, 4),
(5, 5),
(5, 13),
(5, 15),
(6, 4),
(6, 5),
(6, 6),
(6, 14),
(7, 5),
(7, 6),
(7, 7),
(7, 15),
(8, 6),
(8, 7),
(8, 8),
(9, 7),
(9, 8),
(9, 9),
(10, 8),
(10, 9),
(10, 10),
(11, 9),
(11, 10),
(11, 11),
(12, 10),
(12, 11),
(12, 12);

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

CREATE TABLE `film` (
  `id` int(3) UNSIGNED NOT NULL,
  `titre` varchar(50) NOT NULL,
  `date_sortie` date DEFAULT NULL,
  `episode` int(2) DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `film`
--

INSERT INTO `film` (`id`, `titre`, `date_sortie`, `episode`, `description`, `image`) VALUES
(1, 'THE PHANTON MENACE', '2018-03-20', 1, 'AprÃ¨s un millÃ©naire, un mal ancien revient pour se venger. Pendant ce temps, le chevalier Jedi Qui-Gon Jinn dÃ©couvre Anakin Skywalker: un jeune esclave exceptionnellement fort avec la Force.', 'Phantom-Menace.jpeg'),
(2, 'ATTACK OF THE CLONES', '2018-04-26', 2, 'Suite d\'une tentative d\'assassinat sur le sÃ©nateur PadmÃ© Amidala, les chevaliers Jedi Anakin Skywalker et Obi-Wan Kenobi enquÃªtent sur un mystÃ©rieux complot qui pourrait changer la galaxie pour toujours.', 'Attack-Clones.jpeg'),
(3, 'REVENGE OF THE SITH', '2018-06-30', 3, 'Le diabolique Dark Sidious met en oeuvre son plan final pour un pouvoir illimitÃ© - et l\'hÃ©roÃ¯que Jedi Anakin Skywalker doit choisir un camp.', 'Revenge-Sith.jpeg'),
(4, 'A NEW HOPE', '2018-10-26', 4, 'Avec le pouvoir destructeur de planÃ¨te de l\'Ã©toile de la mort, l\'Empire cherche Ã  consolider son emprise sur la galaxie. Pendant ce temps, le garÃ§on de ferme Luke Skywalker se lÃ¨ve pour faire face Ã  son destin.', 'New-Hope.jpeg'),
(5, 'THE EMPIRE STRIKES BACK', '2018-12-25', 5, 'Alors que l\'Ã©toile de la mort a Ã©tÃ© dÃ©truite, la bataille entre l\'Empire et l\'Alliance rebelle fait rage ... et le diabolique Dark Vador poursuit sa recherche incessante de Luke Skywalker.', 'Empire-Strikes-Back.jpeg'),
(6, 'RETURN OF THE JEDI', '2019-01-06', 6, 'Luke Skywalker mÃ¨ne une mission pour sauver son ami Han Solo des griffes de Jabba le Hutt, tandis que l\'empereur cherche Ã  dÃ©truire la rÃ©bellion une fois pour toutes avec une deuxiÃ¨me Ã©toile de la mort redoutÃ©e.', 'return-Jedi.jpeg'),
(7, 'THE FORCE AWAKENS', '2019-08-05', 7, 'Trente ans aprÃ¨s la bataille d\'Endor, une nouvelle menace est apparue sous la forme du Premier Ordre et du mÃ©chant Kylo Ren. Pendant ce temps, Rey, une jeune charognarde, dÃ©couvre des pouvoirs qui vont changer sa vie - et peut-Ãªtre sauver la galaxie.', 'the-force-awakens.jpeg'),
(8, 'THE LAST JEDI', '2020-03-01', 8, 'Rey a trouvÃ© le lÃ©gendaire Luke Skywalker, dans l\'espoir d\'Ãªtre formÃ© aux voies de la Force. Pendant ce temps, le Premier Ordre cherche Ã  dÃ©truire les restes de la RÃ©sistance et Ã  gouverner la galaxie sans opposition', 'the-last-jedi.jpeg'),
(9, 'THE RISE OF SKYWALKER', '2020-10-06', 9, 'Un puissant ennemi revient et Rey doit faire face Ã  son destin.', 'the-rise-of-skywalker.jpeg'),
(10, 'A STAR WARS STORY 1', '2021-01-30', 10, 'De Lucasfilm vient le premier des films autonomes de Star Wars, Â«Rogue One: A Star Wars StoryÂ», une toute nouvelle aventure Ã©pique. En pÃ©riode de conflit, un groupe de hÃ©ros improbables se rÃ©unissent pour voler les plans de l\'Ã‰toile de la mort, l\'arme de destruction ultime de l\'Empire. Cet Ã©vÃ©nement clÃ© de la chronologie de Star Wars rassemble des gens ordinaires qui choisissent de faire des choses extraordinaires et, ce faisant, font partie de quelque chose de plus grand qu\'eux.', 'star1.jpeg'),
(11, 'A STAR WARS STORY 2', '2021-02-12', 11, 'Montez Ã  bord du Millennium Falcon et voyagez dans une galaxie trÃ¨s lointaine dans Solo: A Star Wars Story, une toute nouvelle aventure avec le scÃ©lÃ©rat le plus aimÃ© de la galaxie. Ã€ travers une sÃ©rie d\'escapades audacieuses au cÅ“ur d\'un monde criminel sombre et dangereux, Han Solo rencontre son puissant futur copilote Chewbacca et rencontre le joueur notoire Lando Calrissian, dans un voyage qui marquera le cours de l\'un des hÃ©ros les plus improbables de la saga Star Wars.', 'star2.jpeg'),
(12, 'SHADOW OF VADER\'S CASTLE', '2021-04-24', 12, 'L\'Empire a chutÃ©, et les Mustafariens font la fÃªte. Mais ils savent qu\'ils ne seront jamais en sÃ©curitÃ© tant qu\'ils vivront dans l\'ombre du chÃ¢teau de Vador. Deux jeunes garÃ§ons dÃ©cident d\'aller dÃ©truire eux-mÃªmes le chÃ¢teau. Mais un vieux sage Mustafarien essaie de les en empÃªcher en leur racontant des contes afin qu\'ils apprennent des leÃ§ons qu\'ils auraient, sinon, appris d\'une faÃ§on beaucoup plus dure.', 'shadow_vader_castle.jpg'),
(13, 'BOUNTY HUNTERS', '2021-06-17', 13, 'Un code d\'honneur unit les chasseurs de primes, pourtant reconnus sans foi ni loi. Il se rÃ©sume Ã  une rÃ¨gle : ne jamais trahir un chasseur de primes... surtout si c\'est Boba Fett ! C\'est pourtant ce qu\'a fait Valance lors d\'une mission qui a mal tournÃ©, quand Nakano Lash, le mentor de Valance, les a trahis, lui et son Ã©quipe. Aujourd\'hui, Lash refait surface et tous les chasseurs de primes sont Ã  ses trousses. Valance compte bien prendre sa revanche... mais il lui faudra aussi rÃ©gler ses comptes avec Boba Fett !', 'bountyHunters.jpg'),
(14, 'LIGHT OF THE JEDI', '2021-08-03', 14, 'Lorsqu\'une catastrophe choquante au sein de l\'hyperespace dÃ©chire un vaisseau en morceaux, la rafale de dÃ©bris Ã©mergeant du dÃ©sastre menace un systÃ¨me tout entier. Ã€ peine rÃ©sonne l\'appel Ã  l\'aide que les Jedi se prÃ©cipitent sur les lieux. La portÃ©e de la rafale, cependant, est suffisante pour pousser les Jedi dans leurs retranchements. Alors que le ciel s\'ouvre et que la destruction s\'abat sur l\'alliance pacifique qu\'ils avaient aider Ã  bÃ¢tir, les Jedi doivent croire en la Force en ce jour oÃ¹ une simple erreur pourrait coÃ»ter des milliards de vies.', 'light_of_the_jedi.jpg'),
(15, 'INTO THE DARK', '2021-10-30', 15, 'Le Padawan Reath Silas quitte la capitale galactique cosmopolite de Coruscant et est envoyÃ© vers les frontiÃ¨res non dÃ©veloppÃ©es - et rien n\'aurait pu le rendre plus malheureux. Il prÃ©fÃ©rerait rester au Temple Jedi, Ã  Ã©tudier les archives. Mais lorsque le vaisseau Ã  bord duquel il voyage se retrouve Ã©jectÃ© de l\'hyperespace lors d\'un dÃ©sastre galactique, Reath se retrouve au cÅ“ur de l\'action. Le Jedi et ses compagnons de voyage trouvent refuge sur ce qui semble Ãªtre une station spatiale abandonnÃ©e. Mais d\'Ã©tranges choses ont alors lieu, amenant le Jedi Ã  enquÃªter sur la vÃ©ritÃ© qui se cache Ã  bord de cette station mystÃ©rieuse, une vÃ©ritÃ© qui pourrait mettre fin Ã  la tragÃ©die...', 'into_the_dark.jpg'),
(16, 'Divine', '2020-11-19', 22, 'La vie est simple et belle', 'film1.PNG'),
(18, 'bonjour', '2020-11-04', 54, 'kgfdsfghjkllkjhgfdfghjklkjhgb', 'bateau.png');

-- --------------------------------------------------------

--
-- Structure de la table `personnage`
--

CREATE TABLE `personnage` (
  `id` int(6) UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `image` varchar(30) DEFAULT NULL,
  `univers` int(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `personnage`
--

INSERT INTO `personnage` (`id`, `nom`, `description`, `image`, `univers`) VALUES
(1, 'Yoda', 'Yoda Ã©tait un maÃ®tre Jedi lÃ©gendaire et plus fort que la plupart dans son lien avec la Force. De petite taille mais sage et puissant, il a formÃ© des Jedi pendant plus de 800 ans, jouant un rÃ´le essentiel dans la guerre des clones, les instructions de Luke Skywalker et ouvrant la voie Ã  l\'immortalitÃ©.', 'Yoda.jpg', NULL),
(2, 'Dark Vador', 'Ancien sabre laser qui sert de puissant symbole de leadership aux Mandaloriens, le sabre noir est une arme unique avec une lame d\'Ã©nergie noire bordÃ©e d\'un bord blanc crÃ©pitant.', 'DarkVador.jpg', NULL),
(3, 'Han Solo', 'Han Solo est sorti d\'une enfance pauvre dans les rues mÃ©chantes de Corellia pour devenir l\'un des hÃ©ros de l\'Alliance rebelle. En tant que capitaine du Millennium Falcon, Han et son copilote Chewbacca ont cru en la cause de la libertÃ© galactique, rejoignant Luke Skywalker et la princesse Leia Organa dans la lutte contre l\'Empire. AprÃ¨s la bataille d\'Endor, Han a fait face Ã  des moments difficiles dans une galaxie chaotique, conduisant Ã  une confrontation bouleversante avec son fils sÃ©parÃ© Ben.', 'HanSolo.jpg', NULL),
(4, 'Dark Maul', 'Un vaisseau tout Ã  fait redoutable pour son pilote mortel, le Sith Infiltrator est le vaisseau personnel de Dark Maul, le Seigneur des TÃ©nÃ¨bres des Sith. Maul a utilisÃ© le vaisseau Ã©lÃ©gant pour voyager silencieusement partout oÃ¹ son maÃ®tre, Dark Sidious, ordonnait. L\'infiltrateur s\'est posÃ© sur Tatooine pendant la recherche de la reine Amidala et est devenu une base mobile d\'opÃ©rations pour Maul. Depuis son vaisseau, il a lancÃ© un trio de droÃ¯des sondes Sith pour rechercher la reine et ses protecteurs Jedi. Une fois trouvÃ©, il dÃ©chargea son rapide Sith Speeder pour poursuivre sa carriÃ¨re.', 'DarkMaul.jpg', NULL),
(5, 'Padme Amidala', 'PadmÃ© Amidala Ã©tait un leader courageux et plein d\'espoir, servant de reine puis de sÃ©nateur de Naboo - et Ã©tait Ã©galement Ã  portÃ©e de main avec un blaster. MalgrÃ© ses idÃ©aux et tout ce qu\'elle a fait pour la cause de la paix, son mariage secret et interdit avec Jedi Anakin Skywalker se rÃ©vÃ¨lerait avoir des consÃ©quences dÃ©sastreuses pour la galaxie.', 'PadmeAmidala.jpg', NULL),
(6, 'Aayla Secura', 'Avec une silhouette athlÃ©tique, une beautÃ© exotique et une peau bleue, Aayla Secura s\'est dÃ©marquÃ©e parmi les nombreux visages des rangs Jedi. Guerrier rusÃ© et chevalier Jedi lors de la montÃ©e de la guerre des clones, Aayla a combattu aux cÃ´tÃ©s du commandant clone Bly sur de nombreux champs de bataille exotiques. Ayant maÃ®trisÃ© le dÃ©tachement Ã©motionnel nÃ©cessaire dans l\'Ordre Jedi, elle a toujours essayÃ© de transmettre ce qu\'elle avait appris aux autres. Aayla a Ã©tÃ© tuÃ©e, avec de nombreux autres gÃ©nÃ©raux Jedi, lorsque ses troupes se sont retournÃ©es contre elle en rÃ©action Ã  la diffusion de l\'Ordre 66 par le chancelier suprÃªme Palpatine.', 'aaylasecura.jpeg', NULL),
(7, 'Adi Gallia', 'Le MaÃ®tre Jedi Adi Gallia Ã©tait membre du Haut Conseil de l\'Ordre pendant la Guerre des Clones. Elle et les autres membres de ce corps dirigeant se rÃ©uniraient dans un temple au-dessus du paysage de Coruscant, dÃ©cidant des questions importantes des Jedi. Stern et concentrÃ©e, elle ferait remarquer les tactiques les plus scandaleuses menÃ©es par Anakin Skywalker ou Obi-Wan Kenobi au cours de la guerre, mais condamnerait rarement leurs actions efficaces. Bien que pas aussi impÃ©tueux qu\'Anakin, Adi Gallia Ã©tait connu pour Ãªtre un guerrier agressif qui n\'avait aucun problÃ¨me Ã  amener le combat Ã  l\'ennemi.', 'adigallia.jpeg', NULL),
(8, 'Amiral Coburn', 'Avec un visage sÃ©vÃ¨re, des reflets d\'acier et une prÃ©sence de commandement indÃ©niable, l\'amiral Coburn a servi aux cÃ´tÃ©s du gÃ©nÃ©ral Plo Koon pendant la guerre des clones. L\'officier de flotte taciturne mesura ses mots avec prÃ©cision, offrant des commandes claires dans un accent coupÃ©. La discipline militaire rigide de Coburn lui a bien servi dans l\'exÃ©cution de certaines des missions les plus audacieuses sous la direction des Jedi. Il a commandÃ© une force opÃ©rationnelle de quatre croiseurs dans l\'Ã©paisseur d\'un cordon de flotte sÃ©paratiste au-dessus de Lola Sayu pour faciliter le sauvetage d\'une Ã©quipe de frappe qui a envahi l\'installation de la Citadelle. Coburn a Ã©galement dirigÃ© un croiseur lÃ©ger Jedi pour extraire les prisonniers de l\'installation de traitement des esclaves zygerriens Ã  Kadavo, dirigeant le navire dangereusement prÃ¨s de l\'installation.', 'amiralcoburn.jpeg', NULL),
(9, 'Amiral Garrick Versio', 'Officier de marine accompli qui a amenÃ© son monde natal de Vardos sous le contrÃ´le impÃ©rial, Garrick Versio a Ã©tÃ© rÃ©affectÃ© et promu amiral aprÃ¨s la destruction de l\'Ã©toile de la mort sur Yavin 4. Garrick a formÃ© Inferno Squad pour s\'assurer que rien de la sorte ne se reproduise. La loyautÃ© envers l\'Empire est la premiÃ¨re prioritÃ© de l\'amiral Versio.', 'amiralGarrick.jpeg', NULL),
(10, 'Amiral Griss', 'Officier vÃ©tÃ©ran du Premier Ordre, Frantis Griss commande le Steadfast et son groupement tactique et siÃ¨ge au Conseil suprÃªme. Il entretient d\'excellentes relations avec le gÃ©nÃ©ral Allegiant Pryde, ce qui s\'avÃ¨re utile lorsque le Premier Ordre doit coordonner ses efforts avec l\'armada Sith rÃ©vÃ©lÃ©e Ã  Exegol.', 'amiralGriss.jpeg', NULL),
(11, 'Amiral Karius', 'MÃ©lange intimidant de machine et d\'homme, l\'amiral Karius est le commandant en second de Dark Vador sur Mustafar. SÃ»r et impitoyable, il a l\'intention de veiller personnellement Ã  ce que le capitaine du Windfall ne survive pas Ã  leur sÃ©jour Ã  Fortress Vader.', 'amiralKarius.jpeg', NULL),
(12, 'Amiral Ackbar', 'Commandant vÃ©tÃ©ran, Ackbar a dirigÃ© la dÃ©fense de son monde natal, Mon Cala, pendant la guerre des clones, puis a orchestrÃ© l\'attaque rebelle contre la deuxiÃ¨me Ã©toile de la mort Ã  la bataille d\'Endor. Ackbar rÃ©alisa que les rebelles avaient Ã©tÃ© attirÃ©s dans un piÃ¨ge Ã  Endor, mais ajustÃ©s, sa flotte achetant un temps prÃ©cieux pour que l\'attaque rÃ©ussisse. AprÃ¨s la bataille d\'Endor, Ackbar est devenu un grand amiral de la Nouvelle RÃ©publique, remportant de nombreuses victoires, dont la bataille cruciale de Jakku. Il se retira Ã  Mon Cala, mais fut rÃ©intÃ©grÃ© au service de la RÃ©sistance par Leia Organa.', 'amiralackbar.jpeg', NULL),
(14, 'poiuyt', 'mlkjhgfdfghjklmlkjhgfdcdfghjklm', 'couverture.jpg', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(6) UNSIGNED NOT NULL,
  `nom` varchar(40) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email` varchar(120) CHARACTER SET utf8 NOT NULL,
  `password` varchar(120) CHARACTER SET utf8 NOT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `password`, `role`) VALUES
(3, 'LECKPA', 'Shesley', 'shesleyleckpa@yahoo.fr', '$2y$12$wnrpZUbcP/eLO7zA9ruhLOSX0AIxql3FmYFAqTEcCGX3YNolgRoca', 0),
(5, 'MBANGO', 'Berlange', 'berlangembango@gmail.com', '$2y$10$wqUhkxMKTOwRAmlvjrpk2.3LwecTPKCh2CC3bk87A5dbMb/R1YuNm', 1);

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

CREATE TABLE `vote` (
  `id_user` int(6) UNSIGNED NOT NULL,
  `id_film` int(3) UNSIGNED NOT NULL,
  `vote` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `vote`
--

INSERT INTO `vote` (`id_user`, `id_film`, `vote`) VALUES
(5, 1, 5),
(5, 2, 5),
(5, 3, 3),
(5, 4, 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acteurfilm`
--
ALTER TABLE `acteurfilm`
  ADD PRIMARY KEY (`id_personnage`,`id_film`),
  ADD KEY `id_personnage` (`id_personnage`),
  ADD KEY `id_film` (`id_film`);

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `personnage`
--
ALTER TABLE `personnage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `univers` (`univers`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id_user`,`id_film`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_film` (`id_film`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acteurfilm`
--
ALTER TABLE `acteurfilm`
  MODIFY `id_personnage` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `personnage`
--
ALTER TABLE `personnage`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acteurfilm`
--
ALTER TABLE `acteurfilm`
  ADD CONSTRAINT `fk_acteurFilm_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_acteurFilm_personnage` FOREIGN KEY (`id_personnage`) REFERENCES `personnage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `personnage`
--
ALTER TABLE `personnage`
  ADD CONSTRAINT `fk_personnage_univers` FOREIGN KEY (`univers`) REFERENCES `planete` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `fk_vote_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vote_utilisateur` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
