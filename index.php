<?php
    session_start();
    $titre_page = "STAR WARS - Accueil";
    require_once('header.inc.php');
?>
<body class="bg-light">
    <main role="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <h2> Actualités</h2>
                    </div>
                    <?php
                        require_once("param.inc.php");
                        require_once("functions.php");
                        date_default_timezone_set('Europe/Paris');
                        $date_actu = date('Y-m-d');
                        $mysqli = new mysqli($host, $login, $password, $dbname);
                            if ($mysqli->connect_errno){ 
                                echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
                            }else{
                                $result = $mysqli->query("SELECT * FROM film WHERE date_sortie < '$date_actu' AND episode > 3");
                                $id = "actualite";
                                multicarousel($id, $result);
                                $result = $mysqli->query("SELECT * FROM film WHERE date_sortie < '$date_actu' AND episode > 3");
                                createModalF($id, $result);
                            }
                    ?>
                    
                    <div class="row">
                        <h2> Prochaines sorties</h2>
                    </div>
                    <?php
                        
                        date_default_timezone_set('Europe/Paris');
                        $date_actu = date('Y-m-d');
                        $result = $mysqli->query("SELECT * FROM film WHERE date_sortie > '$date_actu'");
                        $id = "prochaines_sorties";
                        multicarousel($id, $result);
                        $result = $mysqli->query("SELECT * FROM film WHERE date_sortie > '$date_actu'");
                        createModalF($id, $result);
                   ?>
                   
                </div>
                <div class="col-md-3 bg-secondary">
                    <div class="row text-center">
                        <h1>STAR WARS</h1>
                    </div>
                    <div class="row text-left bg-danger">
                        <h2>Top Films</h2>
                    </div>
                    <br/>
                    <?php
                        $result1 = $mysqli->query("SELECT id_film, SUM(vote) AS nbvote FROM vote GROUP BY id_film ORDER BY nbvote DESC");
                        $row1 = $result1->fetch_assoc();
                        $d = 0;
                        while($row1 == true && $d < 3){
                            $id_film = $row1['id_film'];
                            $result = $mysqli->query("SELECT * FROM film WHERE id = $id_film");
                            $row=$result->fetch_assoc();
                            while($row == true){
                                ?>
                                <div class="row align-items-center"> 
                                    <div class="col">
                                        <img src="images/<?php echo $row['image']; ?>" height=300px width=280px/>
                                        <h4><?php echo $row['titre']; ?></h4>
                                    </div>
                                </div>
                                <br/>
                                <?php
                                $row=$result->fetch_assoc();
                            }
                            $row1=$result1->fetch_assoc();
                            $d = $d + 1;
                        }  
                    ?>
                </div>
            </div>

        </div>
    </main>
    <?php require_once("footer.inc.php"); ?>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>