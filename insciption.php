<?php
    $titre_page = "STAR WARS - Inscription";
    require('header.inc.php');
?>
<body background="images/imagefond.jpg">
    <h1 class="centerpersonnage">Inscription</h1>
    <div class=" row centerdiv">
        <form method="post" class="center">
            <div class="form-group row">
                <label for=identifiant>Nom</label> <span class="etoile">*</span>
                <input class="form-control" type="text" id="nom" size="50" maxlength="40" name="nom" required/><br />
            </div>
            <div class="form-group row">
                <label for=identifiant>Prénom</label> <span class="etoile">*</span>
                <input class="form-control" type="text" id="prenom" size="50" maxlength="40" name="prenom" required/><br />
            </div>
            <div class="form-group row">
                <label for=identifiant>Adresse mail</label> <span class="etoile">*</span>
                <input class="form-control" type="email" id="email" size="50" maxlength="120" name="email" required/><br />
            </div>
            <div class="form-group row">
                <label for=password>Mot de passe</label> <span class="etoile">*</span>
                <input class="form-control" type="password" id="pass" size="50" maxlength="60" name="pass" required/><br />
            </div>
            <div class="form-group row">
                <label for=password>Confirmer Mot de passe</label> <span class="etoile">*</span> 
                <input class="form-control" type="password" id="cpass" size="50" maxlength="60" name="cpass" required/><br />
            </div>
            <div>
                <input class="btn btn-primary" type="submit" id="formsend" name="formsend" value="Valider"/>
            </div>
            <div class="row"><br/></div>
        </form>
    
    <?php 
        if(isset($_POST['formsend'])){
            extract($_POST);
            if(!empty($nom) && !empty($prenom) && !empty($email) && !empty($pass) && !empty($cpass)){
                if($pass == $cpass){
                    $options = [
                        'cost' => 10
                    ];
                    $hashpass = password_hash($pass, PASSWORD_BCRYPT, $options);
                    require_once("param.inc.php");
                    $role = 0;
                    $mysqli = new mysqli($host, $login, $password, $dbname);
                    if ($mysqli->connect_errno){ 
                        echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
                    }else{
                        $c = $mysqli->prepare("SELECT * FROM utilisateur WHERE email = ?");
                        $c->bind_param('s',$email);
                        $c->execute();
                        $result = $c->get_result();
                        $nbr = mysqli_num_rows($result);
                        if($nbr == 0){
                            $stmt = $mysqli->prepare("INSERT INTO utilisateur(nom, prenom, email, password, role) VALUES (?, ?, ?, ?, ?)");
                            $stmt->bind_param('ssssi',$nom, $prenom, $email, $hashpass, $role);
                            $stmt->execute();
                            ?>
                            <div class = "row center">
                                <div class = "alert alert-success" role = "alert">
                                    Votre compte a été créé ! <a href="index.php">Retour à la page d'accueil</a>
                                </div>
                            </div>  
                            <?php
                        }else{
                            ?>
                            <div class = "row center">
                                <div class = "alert alert-danger" role = "alert">
                                    Cet email existe déja ! <a href="insciption.php">Réessayer</a>
                                </div>
                            </div>  
                            <?php
                        }
                        
                    }
                }else{
                    ?>
                    <div class = "row center">
                        <div class = "alert alert-danger" role = "alert">
                            Mots de passe différents ! <a href="insciption.php">Réessayer</a>
                        </div>
                    </div>  
                    <?php
                }
            }
        }
    
    ?>
    </div>
    <?php require_once("footer.inc.php"); ?>
</body>