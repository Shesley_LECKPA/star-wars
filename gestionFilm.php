<?php
  session_start();
  $titre_page = "STAR WARS - Gestion - Modifier - Film";
  if((!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok") || $_SESSION['role']===0){
    header('Location:index.php');
    exit;
  }
    require('header.inc.php');
?>
<body>
    <?php
        require_once("param.inc.php");
        $mysqli = new mysqli($host, $login, $password, $dbname);
        if(isset($_POST['modifierF'])){
            $id=$_POST['modifierF_id'];
            $result = $mysqli->query("SELECT * FROM film WHERE id = $id");
            $row = $result->fetch_assoc();
    ?>
    <h3 class="centerpersonnage">Modification du film</h3><br>
    <div class="row">
    <form method="POST"  action="modifierFilm.php"  enctype="multipart/form-data" class = "center">
     <div class="form-group row">
       <label for="identifiant">Titre</label>
       <input type="text" class="form-control" name="titre" id="titre" value=<?php echo $row['titre'];?> placeholder="nom du film"  required>
     </div>
     <div class="form-group row">
       <label for="identifiant">Date de sortie</label>
       <input type="date" class="form-control" name="date_sortie" id="date_sortie" value=<?php echo $row['date_sortie'];?> required>
     </div>
     <div class="form-group row">
       <label for="identifiant">Episode</label>
       <input type="text" class="form-control" name="episode" id="episode" value=<?php echo $row['episode'];?> placeholder="numéro de l'épisode"  required>
     </div>
     <div class="form-group row">
       <label for="identifiant">Description</label>
       <textarea class="form-control " name="description" id="description" required> <?php echo $row['description'];?></textarea>
     </div>
     <div class="form-group row">
       <label for="identifiant">Image </label>
       <input type="file" class="form-control" name="image" id="image" required>
     </div>
     <input type="hidden" name="modifierF_id" id="modifierF_id"  value="<?php echo $id;?>">
     <button type="submit" name="valider" id="valider" class="btn btn-primary">Valider</button>
     <button type="submit" name="annuler" id="annuler" class="btn btn-primary">Annuler</button>
    </form>
    </div>
    <?php
        }
    ?>
    <?php require_once("footer.inc.php"); ?>
</body>