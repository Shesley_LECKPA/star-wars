<?php
    ob_start();
    session_start();
    $titre_page = "STAR WARS - Films";
    if(!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok"){
        header('Location:index.php');
        exit;
    }
    require('header.inc.php');
?>
<body>
        <?php
        require_once("param.inc.php");
        $mysqli = new mysqli($host, $login, $password, $dbname);
        if ($mysqli->connect_errno){ 
            echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
        }else{
            $result = $mysqli->query("SELECT * FROM film");
            if(!$result){
                echo "Echec de la requête SQL (" .$mysqli->error. ")"; 
            }elseif($result->num_rows == 0){
                echo "Aucun résultat";
            }else{
                $stmt = $mysqli-> query("SELECT * FROM film");
                $row = $stmt -> fetch_assoc();
                while($row == true){
                    ?>
                    
                    <div class="row  taillerow espacefilm defilement">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12 taillecol">
                            <img class="image-fluid" src="<?php echo 'images/'.$row['image']; ?>" height=100% width=100% />
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-12 cadre">
                            <h4><?php echo $row['titre']; ?></h4>
                            <h6>Episode <?php echo $row['episode']; ?></h6>
                            <p> Résumé: </p> 
                            <p><?php echo $row['description']; ?></p><br/>
                            <p> Date de sortie : <?php echo $row['date_sortie']; ?></p><br/>
                            <h6><p> Liste des personnages </p></h6>
                            <?php
                            $id_film = $row['id'];
                            $result1 = $mysqli->query("SELECT * FROM acteurfilm WHERE id_film = $id_film");
                            $row1 = $result1->fetch_assoc();
                            $a = 1;
                            while($row1 == true){
                                $id_pers = $row1['id_personnage'];
                                $result2 = $mysqli->query("SELECT * FROM personnage WHERE id = $id_pers");
                                $row2 = $result2->fetch_assoc();
                                while($row2 == true){
                                    if($a == 1){
                                        $a = 0;
                                        echo $row2['nom'];
                                    }else{
                                        echo ", ".$row2['nom'];
                                    }
                                    $row2 = $result2->fetch_assoc();  
                                     
                                }
                                
                                $row1 = $result1->fetch_assoc();
                            }
                            ?>
                        </div>
                    </div>
                        
                        
                    
                    <?php
                   $row = $stmt -> fetch_assoc(); 
                    
                }
                                
            }
        } 
        
?> 
<?php require_once("footer.inc.php"); ?>
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>