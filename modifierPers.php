<?php
        session_start();
        function test_input($data){
            $data= trim($data);
            $data= stripslashes($data);
            $data= htmlspecialchars($data);
            return $data;
        }

        if(isset($_POST['valider'])){
            $image = $_FILES["image"]['name'];
            $id=$_POST['modifier_id'];
            extract($_POST);
            if(!empty($nom) && !empty($description)){
                $nom= test_input($nom);
                $description= test_input($description);
                if(file_exists("images/".$_FILES['image']['name'])){
                    define('HOST','localhost');
                    define('DB_NAME','starwars');
                    define('USER','root');
                    define('PASS','root');
            
                    try{
                        $db = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME, USER,  PASS);
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        
                    } catch(PDOException $e){
                        echo $e;
                    }
            
                        $data = $db->prepare(" UPDATE personnage SET nom=:nom , description=:description , image=:image WHERE id=:id");
                        $data->execute([	
                            'nom'=>$nom,                                                                        
                            'description'=>$description,
                            'image'=>$image,
                            'id'=>$id
                        ]);
                        move_uploaded_file($_FILES['image']['name'], "images/".$_FILES['image']['name']);
                        header('Location: gestion.php');
                }
            }
        }
        if(isset($_POST['annuler'])){
            header('Location: gestion.php');
        }
?>