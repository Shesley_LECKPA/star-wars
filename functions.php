<?php
    function createModalF($id, $result){
        $row=$result->fetch_assoc();
        while($row==true){
            echo "
                <!-- The Modal -->
                <div class='modal fade' id='".$id.$row['id']."Modal' >
                    <div class='modal-dialog modal-dialog-centered modal-dialog-scrollable'>
                        <div class='modal-content'>

                            <!-- Modal Header -->
                            <div class='modal-header bg-danger'>
                                <h4 class='modal-title'>Description</h4>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class='modal-body'>
                                <h5>".$row['titre']." </h5>
                                <h6>Episode ".$row['episode']."</h6><br>
                                <p> Résumé: </p> 
                                <p>".$row['description']."</p> <br>
                                <p> Date de sortie : ".$row['date_sortie']."</p>
                            </div>

                        </div>
                    </div>
                </div>";
            $row=$result->fetch_assoc();
        }
    }
    function multicarousel($id, $result) {
        $a=0;
        echo "<div id='" .$id. "' class='carousel slide' data-ride='carousel'>";
            echo "<div class='carousel-inner'>";
            do{
                if($a == 0){
                    echo "<div class='carousel-item active'>";
                    $a=1;
                    $row=$result->fetch_assoc();
                }else{
                    echo "<div class='carousel-item'>";
                }
                $b=0;
                echo "<div class='row'>";
                while($row==true && $b<3){
                    echo "<div class='col-md-4 text-center'>";
                    echo "<a href='#' data-toggle='modal' data-target='#".$id.$row['id']."Modal'><img src='images/" .$row['image']. "' height=350px width=280px/></a>";
                    echo "<h4>" .$row['titre']. "</h4>";
                    echo "</div>";
                    $b = $b + 1;
                    $row=$result->fetch_assoc();
                }
                echo "</div>";
                echo "</div>";
            }while($row ==true);
            echo "</div>";
            echo "<a class='carousel-control-prev' href='#" .$id. "' data-slide='prev'>";
            echo "<span class='carousel-control-prev-icon'></span>";
            echo "</a>";
            echo "<a class='carousel-control-next' href='#" .$id. "' data-slide='next'>";
            echo "<span class='carousel-control-next-icon'></span>";
            echo "</a>";
        echo "</div>";

    }
?>
