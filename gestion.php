<?php
  session_start();
  $titre_page = "STAR WARS - Gestion";
  if((!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok") || $_SESSION['role']===0){
    header('Location:index.php');
    exit;
  }
    require('header.inc.php');
?>
<body> 
<div class="row espacefilm">
  <div class="col-3 bg-light">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-personnages-list" data-toggle="list" href="#list-personnages" role="tab" aria-controls="personnages">Personnages</a>
      <a class="list-group-item list-group-item-action" id="list-films-list" data-toggle="list" href="#list-films" role="tab" aria-controls="films">Films</a>
      <a class="list-group-item list-group-item-action" id="list-utilisateurs-list" data-toggle="list" href="#list-utilisateurs" role="tab" aria-controls="utilisateurs">Utilisateurs</a>
      <a class="list-group-item list-group-item-action" id="list-votes-list" data-toggle="list" href="#list-votes" role="tab" aria-controls="votes">Votes</a>
      <a class="list-group-item list-group-item-action" id="list-ajouterfilm-list" data-toggle="list" href="#list-ajouterfilm" role="tab" aria-controls="ajouterfilm">Ajouter un film</a>
      <a class="list-group-item list-group-item-action" id="list-ajouterpersonnage-list" data-toggle="list" href="#list-ajouterpersonnage" role="tab" aria-controls="ajouterpersonnage">Ajouter un personnage</a>
      <a class="list-group-item list-group-item-action" id="list-ajouterutilisateur-list" data-toggle="list" href="#list-ajouterutilisateur" role="tab" aria-controls="ajouterutilisateur">Ajouter un utilisateur</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-personnages" role="tabpanel" aria-labelledby="list-personnages-list">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Nom</th>
              <th scope="col">Description</th>
              <th scope="col">Image</th>
              <th scope="col">Modification</th>
              <th scope="col">Suppression</th>
            </tr>
          </thead>
          <tbody>
          <?php
            require_once("param.inc.php");
            $mysqli = new mysqli($host, $login, $password, $dbname);            
            if ($mysqli->connect_errno){ 
                echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
            }else{
              $result = $mysqli->query("SELECT * FROM personnage");
              if(!$result){
                  echo "Echec de la requête SQL (" .$mysqli->error. ")"; 
              }elseif($result->num_rows == 0){
                  echo "Aucun résultat";
              }else{
                $row = $result->fetch_assoc();
                while($row == true){                        
                  echo '<tr>
                    <td>'.$row['id'].'</td>
                    <td>'.$row['nom'].'</td>
                    <td>'.$row['description'].'</td>
                    <td><img src="images/'.$row['image'].'" height=100px width=100px /></td>
                    <td><form method="POST" action="gestionPers.php">
                        <input type="hidden" name="modifier_id" id="modifier_id"  value="'.$row["id"].'">
                          <button type="submit" name="modifier" class="btn btn-light">Modifier</button>
                        </form></td>
                    <td><form method="POST" action="supprimerPers.php">
                        <input type="hidden" name="supprimer_id" id="supprimer_id"  value="'.$row["id"].'">
                          <button type="submit" class="btn btn-light" name="supprimer">Supprimer</button>
                        </form></td>
                  </tr>';
                  $row = $result->fetch_assoc();
                }
              }
            }
          ?>
          </tbody>
        </table>
      </div>
      <div class="tab-pane fade" id="list-films" role="tabpanel" aria-labelledby="list-films-list">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Titre</th>
              <th scope="col">episode</th>
              <th scope="col">Description</th>
              <th scope="col">Sortie</th>
              <th scope="col">Image</th>
              <th scope="col">Modification</th>
              <th scope="col">Suppression</th>
            </tr>
          </thead>
          <tbody>
          <?php
            require_once("param.inc.php");
            $mysqli = new mysqli($host, $login, $password, $dbname);            
            if ($mysqli->connect_errno){ 
                echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
            }else{
              $result = $mysqli->query("SELECT * FROM film");
              if(!$result){
                  echo "Echec de la requête SQL (" .$mysqli->error. ")"; 
              }elseif($result->num_rows == 0){
                  echo "Aucun résultat";
              }else{
                $row = $result->fetch_assoc();
                while($row == true){                        
                  echo '<tr>
                    <td>'.$row['id'].'</td>
                    <td>'.$row['titre'].'</td>
                    <td>'.$row['episode'].'</td>
                    <td>'.$row['description'].'</td>
                    <td>'.$row['date_sortie'].'</td>
                    <td><img src="images/'.$row['image'].'" height=100px width=100px /></td>
                    <td><form method="POST" action="gestionFilm.php">
                        <input type="hidden" name="modifierF_id" id="modifierF_id"  value="'.$row["id"].'">
                          <button type="submit" name="modifierF" class="btn btn-light">Modifier</button>
                        </form></td>
                    <td><form method="POST" action="supprimerFilm.php">
                        <input type="hidden" name="supprimerF_id" id="supprimerF_id"  value="'.$row["id"].'">
                          <button type="submit" class="btn btn-light" name="supprimerF">Supprimer</button>
                        </form></td>
                  </tr>';
                  $row = $result->fetch_assoc();
                }
              }
            }
          ?>
          </tbody>
        </table>
      </div>   
      <div class="tab-pane fade" id="list-utilisateurs" role="tabpanel" aria-labelledby="list-utilisateurs-list">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Nom</th>
              <th scope="col">Prénom</th>
              <th scope="col">Adresse mail</th>
              <th scope="col">Role</th>
            </tr>
          </thead>
          <tbody>
          <?php
            require_once("param.inc.php");
            $mysqli = new mysqli($host, $login, $password, $dbname);            
            if ($mysqli->connect_errno){ 
                echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
            }else{
              $result = $mysqli->query("SELECT * FROM utilisateur");
              if(!$result){
                  echo "Echec de la requête SQL (" .$mysqli->error. ")"; 
              }elseif($result->num_rows == 0){
                  echo "Aucun résultat";
              }else{
                $row = $result->fetch_assoc();
                while($row == true){                        
                  echo '<tr>
                    <td>'.$row['id'].'</td>
                    <td>'.$row['nom'].'</td>
                    <td>'.$row['prenom'].'</td>
                    <td>'.$row['email'].'</td>
                    <td>'.$row['role'].'</td>
                  </tr>';
                  $row = $result->fetch_assoc();
                }
              }
            }
          ?>
          </tbody>
        </table>
      </div>
      <div class="tab-pane fade" id="list-votes" role="tabpanel" aria-labelledby="list-votes-list">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Identifiant</th>
                <th scope="col">Nom du film</th>
                <th scope="col">Nombre de votes</th>
              </tr>
            </thead>
            <tbody>
            <?php
              require_once("param.inc.php");
              $mysqli = new mysqli($host, $login, $password, $dbname);            
              if ($mysqli->connect_errno){ 
                  echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error; 
              }else{
                $result1 = $mysqli->query("SELECT id_film, SUM(vote) AS nbvote FROM vote GROUP BY id_film ORDER BY nbvote DESC");
                if(!$result1){
                    echo "Echec de la requête SQL (" .$mysqli->error. ")"; 
                }elseif($result1->num_rows == 0){
                    echo "Aucun résultat";
                }else{
                  $row1 = $result1->fetch_assoc();
                  while($row1 == true){  
                    $id_film = $row1['id_film'];
                    $result = $mysqli->query("SELECT * FROM film WHERE id = $id_film");
                    $row=$result->fetch_assoc();
                    while($row == true){
                      echo '<tr>
                      <td>'.$row['id'].'</td>
                      <td>'.$row['titre'].'</td>
                      <td>'.$row1['nbvote'].'</td>
                    </tr>';
                        $row=$result->fetch_assoc();
                    }                      
                    $row1 = $result1->fetch_assoc();
                  }
                }
              }
            ?>
            </tbody>
          </table>
      </div>
      <div class="tab-pane fade" id="list-ajouterfilm" role="tabpanel" aria-labelledby="list-ajouterfilm-list">
        <form method="POST" class="center" action="Ajouterfilms_gestion.inc.php" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="titre">Titre</label>
            <input class="form-control" type="text" id="titre" size="50" maxlength="40" name="titre" placeholder="titre du film"/><br />
          </div>
          <div class="form-group row">
            <label for="date_sortie">Date de sortie</label>
            <input class="form-control" type="date" id="date_sortie" size="50" maxlength="40" name="date_sortie"/><br />
          </div>
          <div class="form-group row">
            <label for="episode">Episode</label>
            <input class="form-control" type="number" id="episode" size="50" maxlength="120" name="episode" placeholder="episode"/><br />
          </div>
          <div class="form-group row">
            <label for="description">Description</label>
            <input class="form-control" type="textarea" id="description" name="description" /><br />
          </div>
          <div class="form-group row">
            <label for="image">Image du film: </label> <br>
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="250000" maxlength="60" id="image" /> <br /> -->
            <input type="file" name="image" id="image" /> <br />
          </div>
          <div>
            <input class="btn btn-primary" type="submit" id="ajoutfilm" name="ajoutfilm" value="Ajouter"/>
          </div>
        </form>
      </div>
      <div class="tab-pane fade" id="list-ajouterpersonnage" role="tabpanel" aria-labelledby="list-ajouterpersonnage-list">
        <form method="post" class="center" action="Ajouterpersonnage_gestion.inc.php" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="nom">Nom</label>
            <input class="form-control" type="text" id="nom" size="50" maxlength="40" name="nom" placeholder="Nom du personnage"/><br />
          </div>
          <div class="form-group row">
            <label for="description">Description</label>
            <input class="form-control" type="textarea" id="description" size="50" maxlength="40" name="description" /><br />
          </div>
          <div class="form-group row">
            <label for="image">Image du personnage: </label> <br>
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="250000" maxlength="60" id="image" /> <br /> -->
            <input type="file" name="image" id="image" /> <br />
          </div>
          <div>
            <input class="btn btn-primary" type="submit" id="ajoutpersonnage" name="ajoutpersonnage" value="Ajouter"/>
          </div>
        </form>
      </div>
      <div class="tab-pane fade" id="list-ajouterutilisateur" role="tabpanel" aria-labelledby="list-ajouterutilisateur-list">
        <form method="post" class="center" action="ajouterutilisateur_gestion.inc.php">
          <div class="form-group row">
            <label for="nom">Nom</label> 
            <input class="form-control" type="text" id="nom" maxlength="40" name="nom" /><br />
          </div>
          <div class="form-group row">
            <label for="prenom">Prénom</label> 
            <input class="form-control" type="text" id="prenom" maxlength="40" name="prenom" /><br />
          </div>
          <div class="form-group row">
            <label for="email">Adresse mail</label> 
            <input class="form-control" type="email" id="email" maxlength="120" name="email" /><br />
          </div>
          <div class="form-group row">
            <label for="pass">Mot de passe</label> 
            <input class="form-control" type="password" id="pass" maxlength="60" name="pass" /><br />
          </div>
          <div>
            <label>Rôle</label> <br />
            <input class="form-check-input" type="radio" id="role0" name="role" value=0 /> <label class="form-check-label" for="role0">Inscrit</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input class="form-check-input" type="radio" id="role1" name="role" value=1 /> <label class="form-check-label" for="role1">Administrateur</label>
          </div>
          <div>
            <input class="btn btn-primary" type="submit" id="ajouterutilisateur" name="ajoututilisateur" value="Ajouter"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php require_once("footer.inc.php"); ?>
<!-- jQuery CDN - Slim version (=without AJAX) -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>