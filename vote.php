<?php
    session_start();
    define('HOST','localhost');
        define('DB_NAME','starwars');
        define('USER','root');
        define('PASS','root');

        try{
            $db = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME, USER,  PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        } catch(PDOException $e){
            echo $e;
        }
    global $db;
    $titre_page = "STAR WARS - Vote";
    if(!isset($_SESSION['connect']) || $_SESSION['connect'] != "ok"){
        header('Location:index.php');
        exit;
    }
    require('header.inc.php');
?>
<div class="container-fluid ">
    <div class="row" >
<?php       
   
   $c = $_SESSION['email'];
   
   $controle2 = $db->prepare(" SELECT * FROM utilisateur WHERE email = :email ");
   $controle2->execute([
       'email'=> $c       
   ]);
    

   $utilisateur = $controle2->fetch(); 



$controle1 = $db->prepare("SELECT * FROM film");
$controle1->execute([]);
while( $film = $controle1->fetch()){
       
                                        $controle3= $db->prepare(" SELECT vote FROM vote where (id_user = :id_user and id_film = :id)");
                                        $controle3->execute([
                                            
                                            'id_user'=> $_SESSION['id_user'],
                                            'id'=> $film['id'],											 
                                        ]);

                                        $note=$controle3->fetch();
                                            $resultat3 = $controle3->rowCount();
                                               if(($resultat3 == 0) ){?>








        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center espacefilm">
            <h2> <span class="badge badge-white"><?php echo $film['titre'];?></span></h2>
            <?php echo'<img src="images/'.$film['image'].'"height=400px width=300px">'?>
            <div class="cadre couleurvote">
                <h3>Quelle est la qualité de ce film?</h3>       

                <div class="row justify-content-center">
                    <span class="fas fa-star" data-star="1"></span>
                    <span class="fas fa-star" data-star="2"></span>
                    <span class="fas fa-star" data-star="3"></span>
                    <span class="fas fa-star" data-star="4"></span>
                    <span class="fas fa-star" data-star="5"></span>
                    <span class="rating"></span> <br />
                </div>
                <form method="post" action="films.inc.php" >                
                    <input type="hidden" name="rating" id="vote">
                    <input type="hidden" value="<?php echo $_SESSION['id_user'] ?>" name="id_user">
                    <input type="hidden" value="<?php echo $film['id'] ?>" name="id_film">
                    <div class="row justify-content-center">
                        <input class="btn btn-primary" type="submit" value="Envoyer" name="voter" id="voter">
                    </div>
                </form>                                
            </div>
        </div>
    

<?php } }?>
   
<?php       

$controle2 = $db->prepare(" SELECT * FROM utilisateur where email = :email ");
$controle2->execute([

'email'=> $_SESSION['email']

]);


$utilisateur = $controle2->fetch(); 



$controle1 = $db->prepare("SELECT * FROM film");
$controle1->execute([]);
while( $film = $controle1->fetch()){

                   $controle3= $db->prepare(" SELECT vote FROM vote where (id_user = :id_user and id_film = :id)");
                   $controle3->execute([
                       
                       'id_user'=> $_SESSION['id_user'],
                       'id'=> $film['id'],											 
                   ]);

                   $note=$controle3->fetch();
                       $resultat3 = $controle3->rowCount();
                          if(($resultat3 != 0) ){?>







        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center espacefilm">
            <h2> <span class="badge badge-white"><?php echo $film['titre'];?></span></h2>
            <?php echo'<img src="images/'.$film['image'].'"height=400px width=300px">'?>
            <div class="cadre couleurvote">
                <h3>Vous avez donné <?php echo $note['vote'];?> étoiles à ce film</h3>
            </div>   
        </div>

<?php } }?>
    </div>
</div>
<?php require_once("footer.inc.php"); ?>
</body>
</html>